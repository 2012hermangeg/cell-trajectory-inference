###################################
### Plots results #################
###################################


col_pop = [:green, :cyan, :darkorange, :dodgerblue, :purple, :darkred];
arr_pop = ["SLAM", "MPP", "LSK~","LK~","MkP", "MK" ]

list_comp = ["SLAM","LSK", "LK", "MK"]
col_comp = [:green, :darkorange, :dodgerblue, :darkred];
col_comp_bis = [:darkgreen, :darkorange, :blue]
nb_pop = length(col_pop)

list_col =  [
            :black, :red, :green, #J2
            :black, :red, :green, #J1
            :black, :red, :green, #J4
            :black, :red, :green #J3     
        ];

col_genotypes = [:black,:red,:green] #WT, T1, T2
col_genotypes_bis = [:grey, :maroon, :seagreen]
list_style =  [
            :dot, :dot, :dot, #J2
            :dash, :dash, :dash, #J1
            :solid,  :solid,  :solid, #J4
            :dashdot,  :dashdot,  :dashdot #J3   
        ];



function portion_of_disc(l, angle)
    theta = LinRange(-angle,angle, 500)
    x = vcat([0.0], l*cos.(theta), [0.0])
    y = vcat([0.0],  l*sin.(theta), [0.0]) 
    
   return x,y
end


###########################################
### Plots according to selected options ###
###########################################

#Comment / Decomment whether you want to create and save plots
#choice_plot = ""

###########################################
### Repertory: Pre_treatment
###########################################

#choices in [pre_treatment_UMAP, ex_trails_UMAP, corr_occup_LK_over_length]

if choice_plot == "pre_treatment_UMAP"

  nb_cells_UMAP = 3_000

  for m in list_mice
   
    X = asinh.(convert(Matrix, df[m][:,cols]))
    mean_X = mean(X, dims=1)
    X ./= mean_X
    Random.seed!(1)
    proj_UMAP = UMAP_(X', 2, n_neighbors=20)
    
    Random.seed!(1)
    ind_UMAP = shuffle!(collect(1:N_cells[m]))[1:min(N_cells[m],nb_cells_UMAP)]
    for p in 1:nb_pop
      scatter(proj_UMAP.embedding[1,ind_UMAP], proj_UMAP.embedding[2,ind_UMAP],
        markersize=1, label="", alpha=0.5,
        color=:grey)
         
      ind_UMAP_sub = df[m][!,:pop].==p
      scatter!(proj_UMAP.embedding[1,ind_UMAP_sub], proj_UMAP.embedding[2,ind_UMAP_sub],
        markersize=3, label="",
        xlabel="UMAP 1", ylabel="UMAP 2",
        color=col_pop[p],
        title=string(m," - ", pops[p])
       )
    
      savefig(string("fig/pre_treatment/UMAP_",replace(m," "=>"_"),"_",pops[p],".svg"))
    end
  end
end

if choice_plot == "pre_treatment_hist"

end

#####################################
if choice_plot == "ex_trails_UMAP"
  ## Exemple traj
  m_ex = "WT 206"
  X = asinh.(convert(Matrix, df[m_ex][:,cols]))
    Random.seed!(1)
    proj_UMAP = UMAP_(X', 2, n_neighbors=20)

  for num_traj in 1:50
  cellules_traj = Traj_red[m_ex][ind_final_trails[m_ex]][num_traj]

  Random.seed!(1)
    ind_UMAP = shuffle!(collect(1:N_cells[m_ex]))[1:min(N_cells[m_ex],5_000)]
    scatter(proj_UMAP.embedding[1,ind_UMAP], proj_UMAP.embedding[2,ind_UMAP],
        markersize=1, label="", alpha=0.5,
        color=:grey)

  for i in 2:length(cellules_traj)
    
    ind1 = cellules_traj[i-1]
    ind2 = cellules_traj[i]
    
    if ind2 == 0
        break
    end
    
    plot!([proj_UMAP.embedding[1,ind1],proj_UMAP.embedding[1,ind2]],
        [proj_UMAP.embedding[2,ind1],proj_UMAP.embedding[2,ind2]],
        label="", color=:black,
        linewidth = 3
        )

        
    
  end

  scatter!(proj_UMAP.embedding[1,cellules_traj], proj_UMAP.embedding[2,cellules_traj], markersize=3, label="",
    marker_z= pop_cells[m_ex][cellules_traj],
    color = color =cgrad([:green,:cyan,:darkorange, :dodgerblue,:purple,:darkred], 6, categorical = true),
    title="",
    xlabel="UMAP 1", ylabel="UMAP 2",
    )
    
  savefig(string("fig/standard/ex/UMAP_traj_ex_",num_traj,".svg"))
  end

end


###########################################
if choice_plot == "corr_occup_LK_over_length"

  # Correlation between max trail length and %occupation LK~
  for mouse in list_mice
    
    mean_occup_LK = 100 .*[dict_trails_expansion_d[mouse][i,4].-dict_trails_expansion_d[mouse][i,3] for i in 1:nb_trails_per_mouse]
    length_trail = [dict_trails_D[mouse][i][end] for i in 1:nb_trails_per_mouse]
    
    A = hcat(length_trail, mean_occup_LK)
    x = collect(1:maximum(length_trail))
    plot(x,[cor(A[A[:,1] .<x[i],:])[1,2] for i in 1:length(x)],
        xlabel="Max. trail length", ylabel="Correlation", label="",
    title=string(mouse), linewidth=2
    )
    savefig(string("fig/pre_treatment/corr_occup_LK_over_length_",replace(mouse," "=>"_")))
  end

end


###########################################
### Repertory: filtre_max
###########################################


if choice_plot == "corr_occup_LK_over_length_tresh"
  # Same as above plus vertical line
  for mouse in list_mice
    
    mean_occup_LK = 100 .*[dict_trails_expansion_d[mouse][i,4].-dict_trails_expansion_d[mouse][i,3] for i in 1:nb_trails_per_mouse]
    length_trail = [dict_trails_D[mouse][i][end] for i in 1:nb_trails_per_mouse]
    
    A = hcat(length_trail, mean_occup_LK)
    x = collect(1:maximum(length_trail))
    plot(x,[cor(A[A[:,1] .<x[i],:])[1,2] for i in 1:length(x)],
        xlabel="Max. trail length", ylabel="Correlation", label="",
    title=string(mouse), linewidth=2
    )
    vline!([length_trail_lim[mouse]], color=:black, linewidth=2, label="")
    savefig(string("fig/filtre_max/corr_occup_LK_over_length_tresh_",replace(mouse," "=>"_")))
  end
end


#####################################
if choice_plot == "scatter_length_occupLK_filterMax"

  # Scatter plot, trail length vs %occupation
  for mouse in list_mice
    mean_occup_LK = 100 .*[dict_trails_expansion_d[mouse][i,4].-dict_trails_expansion_d[mouse][i,3] for i in 1:nb_trails_per_mouse]
    length_trail = [dict_trails_D[mouse][i][end] for i in 1:nb_trails_per_mouse]
    
    scatter(length_trail, mean_occup_LK, label="",
    xlabel="Trail length", ylabel="Occupation LK~ [%]", 
    title=string(mouse), markersize=1,
    )
    vline!([length_trail_lim[mouse]], color=:black, linewidth=2, label="")
    savefig(string("fig/filtre_max/scatter_length_occupLK_filterMax",replace(mouse," "=>"_")))
  end

end

#####################################
if choice_plot == "length_trail_max"

  for mouse in list_mice
    histogram([dict_trails_D[mouse][i][end] for i in 1:nb_trails_per_mouse], 
        label="",
    xlabel="Trail length", ylabel="nb of trails", 
    title=string(mouse)
    )
     vline!([length_trail_lim[mouse]], color=:black, linewidth=2, label="")
    savefig(string("fig/filtre_max/length_trail_max_",replace(mouse," "=>"_")))
  end

end


#####################################
if choice_plot == "occup_LK_filter_max"
  for mouse in list_mice
    
    mean_occup_LK = 100 .*[dict_trails_expansion_d[mouse][i,4].-dict_trails_expansion_d[mouse][i,3] for i in 1:nb_trails_per_mouse]

    
    histogram(mean_occup_LK[ind_filtre_max[mouse]], 
        label="",
    xlabel="occupation LK~ [%]", ylabel="pdf", 
    title=string(mouse, " (too long trails excluded)"), normalize=:pdf
    )
    savefig(string("fig/filtre_max/occup_LK_filter_max_",replace(mouse," "=>"_")))
  end
end

###########################################
### Repertory: filtre_min
###########################################


if choice_plot == "occup_LK_final_trails"
  for mouse in list_mice
    mean_occup_LK = 100 .*[dict_trails_expansion_d[mouse][i,4].-dict_trails_expansion_d[mouse][i,3] for i in 1:nb_trails_per_mouse]

    
    histogram(mean_occup_LK[ind_final_trails[mouse]], 
        label="",
    xlabel="Occupation LK~ [%]", ylabel="pdf", 
    title=string(mouse, " (filtered trails)"), normalize=:pdf
    )
    savefig(string("fig/filtre_min/occup_LK_final_trails_",replace(mouse," "=>"_")))
  end
end

#####################################
if choice_plot == "scatter_length_occupLK_filterAll"
  # Scatter plot, taille traj vs %occupation
  for mouse in list_mice
    mean_occup_LK = 100 .*[dict_trails_expansion_d[mouse][i,4].-dict_trails_expansion_d[mouse][i,3] for i in 1:nb_trails_per_mouse]
    length_trail = [dict_trails_D[mouse][i][end] for i in 1:nb_trails_per_mouse]

    scatter(length_trail, mean_occup_LK, label="",
    xlabel="Trail length", ylabel="Occupation LK~ [%]", 
    title=string(mouse), markersize=1,
    )
    
    x = collect(floor(Int,threshold_filter_min* length_trail_lim[mouse]):length_trail_lim[mouse])
    prop_lim = 100 .*[threshold_filter_min* length_trail_lim[mouse]/x[i] for i in 1:length(x)]
    plot!(x,prop_lim, color=:black, linewidth=2, linestyle=:dash, label="", ylims=(-2,102),
    #    xlims=(-5,length_trail_lim[mouse]+5)
    )
    vline!([length_trail_lim[mouse]], color=:black, linewidth=2, label="")
    savefig(string("fig/filtre_min/scatter_length_occupLK_filterAll_",replace(mouse," "=>"_")))
  end


end

#####################################
if choice_plot == "hist_cells_filtering"
  println("Show nb of cells explored over selected trails")
  for m in list_mice
    histogram(pop_cells[m][df[m][!,:in_selected_trails].>0].+0.5, 
        bins=collect(0:6), label="",
        color=col_pop,
        xticks=(collect(0.5:5.5), arr_pop),
        ylabel="Nb of cells",
        title=string(m," (over selected trails)")
        )
    savefig(string("fig/filtre_min/hist_cells_filterAll_",replace(m," "=>"_")))
    
    res = [sum(pop_cells[m][df[m][!,:in_selected_trails].>0] .== c) for c in collect(0:5)]
    @show (m,res)
  end
end


###########################################
### Repertory: exploratory
###########################################

if choice_plot == "exploratory_path_ind"

  for i in 1:length(list_mice)
    m = list_mice[i]

    plot(arr_r, expansion[m], 
        yticks=([0,1/3,2/3,1], list_comp ),
        linewidth=3, color=:black, label="original")

    plot!(xlabel="r", ylabel="Expected compartment", title=m)

    plot!(map_r[list_batch[i]], expansion[m], 
         linewidth=2, color=:darkblue, linestyle=:dash, label="r-axis corrected", legend=:bottomright)

    savefig(string("fig/exploratory/exploratory_",replace(m," "=>"_")))
  end

end


#####################################
if choice_plot == "exploratory_path_all"

  ### Without realigning r-axis
  plot()
  for i in 1:length(list_mice)
    m = list_mice[i]

    plot!( arr_r,expansion[m], 
        yticks=([0,1/3,2/3,1], list_comp ),
        linewidth=3, color=list_col[i], linestyle=list_style[i], label="")

  end

  plot!(xlabel="r", ylabel="Expected compartment", title="Original")
  savefig(string("fig/exploratory/exploratory_all"))

  ### With corrected r-axis
  plot()
  for i in 1:length(list_mice)
    m = list_mice[i]

    plot!( map_r[list_batch[i]],expansion[m], 
        yticks=([0,1/3,2/3,1], list_comp ),
        linewidth=3, color=list_col[i], linestyle=list_style[i], label="")

  end

  plot!(xlabel="r", ylabel="Expected compartment", title="r-axis corrected")
  savefig(string("fig/exploratory/exploratory_all_corr"))

end


#####################################
if choice_plot == "exploratory_path_WT_pool"


  plot()
  for i in 1:length(list_WT)
    m = list_WT[i]

    plot!( arr_r,expansion[m], 
        yticks=([0,1/3,2/3,1], list_comp ),
        linewidth=2, color=:black, linestyle=list_style[1+(i-1)*3], label="")

  end

  plot!(arr_r,expansion["pool_WT"] , linewidth=4, color=:purple, label="pool", legend=:bottomright)

  for i in 1:length(list_WT)
    m = list_WT[i]
    
    plot!(map_r[list_days[i]], expansion[m], label="", linestyle=:dash, color=:darkblue)

  end


  plot!(xlabel="r", 
    ylabel="Expected compartment", 
    title="WT",
    yticks=([0,1/3,2/3,1], list_comp )
    )
  savefig(string("fig/exploratory/exploratory_path_WT_pool"))
  
end


###########################################
### Repertory: differentiation
###########################################

if choice_plot == "diff_correction_WT"


  plot(title=string(cols[j]))

  #before y-correction
  for i in 1:length(list_WT)
    m = list_WT[i]

    plot!(arr_r, mean_traj[m], 
        linewidth=2, color=:black, linestyle=list_style[1+(i-1)*3], label="")

  end

  plot!(arr_r,mean_traj["pool_WT"] , linewidth=4, color=:purple, label="pool", legend=:bottomright)

  # After y-correction
  for i in 1:length(list_WT)
    m = list_WT[i]
    
    plot!(arr_r,mean_traj[m].*lambda_j[list_days[i]], label="", linestyle=:dash, color=:darkblue)

  end

  plot!(xlabel="r", ylabel="Intensity", )

  savefig(string("fig/differentiation/correction/correction_",cols[j]))
  
end

#####################################
if choice_plot == "diff_path_all"

  ### Without realigning y-axis
  plot()
  for i in 1:length(list_mice)
    m = list_mice[i]

    plot!( arr_r,mean_traj[m], 
        linewidth=3, color=list_col[i], linestyle=list_style[i], label="")

  end

  plot!(xlabel="r", ylabel="Intensity", title=string(cols[j], " - Original"))
  savefig(string("fig/differentiation/before_corr_y/traj_",cols[j]))

  ### With corrected y-axis
  plot()
  for i in 1:length(list_mice)
    m = list_mice[i]

    plot!(arr_r,mean_traj[m].*lambda_j[list_batch[i]], 
        linewidth=3, color=list_col[i], linestyle=list_style[i], label="")

  end

  plot!(xlabel="r", ylabel="Intensity", title=string(cols[j], " - correction batch"))
  savefig(string("fig/differentiation/after_corr_y/traj_",cols[j]))

end


#####################################
if choice_plot == "diff_path_pool"

  plot()

  plot!(arr_r, mean_traj["pool_WT"], linewidth=3, color=col_genotypes[1], label="")


  pool_T1 = zeros(length(mean_traj["pool_WT"]))
  for i in 1:length(list_T1)
    m = list_T1[i]
    pool_T1 .+= mean_traj[m].*lambda_j[list_days[i]]
  end
  pool_T1 ./= length(list_T1)

  plot!(arr_r, pool_T1, linewidth=3, color=col_genotypes[2], label="")

  pool_T2 = zeros(length(mean_traj["pool_WT"]))
  for i in 1:length(list_T2)
    m = list_T2[i]
    pool_T2 .+= mean_traj[m].*lambda_j[list_days[i]]
  end
  pool_T2 ./= length(list_T2)

  plot!(arr_r, pool_T2, linewidth=3, color=col_genotypes[3], label="")

  plot!(xlabel="r", ylabel="Intensity", title=string(cols[j]))

  savefig(string("fig/differentiation/pool/pool_",cols[j]))
  #col_genotypes = [:black,:red,:green] #WT, T1, T2
end

###########################################
### Repertory: expansion
###########################################

theta = pi/9

if choice_plot == "expansion_path_WT"
  plot(portion_of_disc(1.0, theta), seriestype=[:shape], label="" , color=:darkred)
  plot!(portion_of_disc( mean(sqrt.(pool["WT"][:,3])), theta), 
      seriestype=[:shape], label="" , color=col_comp[3])
  plot!(portion_of_disc( mean(sqrt.(pool["WT"][:,2])), theta), 
        seriestype=[:shape], label="" , color=col_comp[2])
  plot!(portion_of_disc( mean(sqrt.(pool["WT"][:,1])), theta), 
            seriestype=[:shape], label="" , color=col_comp[1])

  plot!( grid=false, axis=false, background_color=:transparent)
  savefig("fig/expansion/disc_WT_ref")
  plot!([0,1], [0,0], color=:black, linestyle=:dash, label="")
  savefig("fig/expansion/disc_WT_ref_withLine")

end

#####################################
if choice_plot == "bar_plot_expansion_path_WT"
  plot()
  for i in 1:3
    plot!([-0.,i],mean(sqrt.(pool["WT"][:,i])) .*[1,1], 
        label="", linestyle=:solid, linewidth=2, color=:black)
  end
  plot!([-0.,3.5],[1,1], label="", linestyle=:solid, linewidth=2, color=:black)
  bar!( [mean(sqrt.(pool["WT"][:,i])) for i in 1:3], color=col_comp[1:3], label="", alpha=0.8)
  for i in 1:3
    scatter!( [i], mean(sqrt.(pool["WT"][:,i])) .*[1,1], 
        color=col_comp_bis[i], label="", xaxis=false, grid=:false
    )
  end
  plot!(ylims=(0,1.02), ymirror = true, ylabel="r", background_color=:transparent)
  savefig("fig/expansion/WT_barPlot")

end

#####################################
little_theta = pi/27
if choice_plot == "ex_trails_WT"
  for i in 1:50
    
    rd_tr = rand(collect(1:size(pool["WT"],1)))
    (r1,r2,r3) = sqrt.(pool["WT"][rd_tr,:])
 
    plot(portion_of_disc(1.0, little_theta), seriestype=[:shape], label="" , color=col_comp[4])
    plot!(portion_of_disc(r3, little_theta), seriestype=[:shape], label="" , color=col_comp[3])
    plot!(portion_of_disc(r2, little_theta), seriestype=[:shape], label="" , color=col_comp[2])
    plot!(portion_of_disc(r1, little_theta), seriestype=[:shape], label="" , color=col_comp[1])
    plot!([0,1], [0,0], color=:black, linestyle=:dash, label="")

    plot!(aspect_ratio=1, grid=false, axis=false, background_color=:transparent)
    savefig(string("fig/expansion/ex_WT/",i))

  end

end

#####################################
if choice_plot == "expansion_path_genotypes"
  #REF SLAM
  r_SLAM_WT = mean(sqrt.(pool["WT"][:,1]))
  d_SLAM_WT = r_SLAM_WT^2

  for genotype in ["T1","T2"]

    d_SLAM = mean((pool[genotype][:,1]))
    d_LSK = mean((pool[genotype][:,2]).-(pool[genotype][:,1]))
    d_LK = mean((pool[genotype][:,3]).-pool[genotype][:,2])

    r_LSK = sqrt((d_SLAM+d_LSK)*d_SLAM_WT/d_SLAM)
    r_LK = sqrt((d_SLAM+d_LSK+d_LK)*d_SLAM_WT/d_SLAM)
    r_MK = sqrt(1.0*d_SLAM_WT/d_SLAM)



   plot(portion_of_disc(r_MK, theta), seriestype=[:shape], label="" , color=col_comp[4])
    plot!(portion_of_disc( r_LK, theta), 
        seriestype=[:shape], label="" , color=col_comp[3])
    plot!(portion_of_disc(r_LSK, theta), 
            seriestype=[:shape], label="" , color=col_comp[2])
    plot!(portion_of_disc( r_SLAM_WT, theta), 
            seriestype=[:shape], label="" , color=col_comp[1])

    plot!( grid=false, axis=false, background_color=:transparent)
    savefig(string("fig/expansion/disc_refSLAM_",genotype))
    plot!([0,1], [0,0], color=:black, linestyle=:dash, label="")
    savefig(string("fig/expansion/disc_refSLAM_withLine_",genotype))
  
    end
end




#######################################
#Reinitialize choice for plots
choice_plot = ""
