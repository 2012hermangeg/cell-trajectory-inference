col_surface_markers = [
    #Symbol("89Y_CD45"), #HSC
    Symbol("164Dy_Sca-1"), #HSC
    Symbol("173Yb_c-Kit"), # HSC
     Symbol("154Sm_CD48"), #HSC
     Symbol("167Er_CD150"), #HSC
    #Symbol("170Er_CD34"), #HSC -> suppr
     Symbol("150Nd_CD135"), #HSC
     Symbol("143Nd_CD41"), #MK lineage 
     Symbol("168Er_MPL"), #MK
     Symbol("151Eu_beads_3_CD42d"), #MK
     Symbol("176Yb_CalR_ext")];

    ########### Intracellular Markers ##############
col_intracellular_markers = [ 
    Symbol("147Sm_P-STAT5"), #Signalling
    Symbol("158Gd_P-STAT3"),  #Signalling
     Symbol("162Dy_STAT3"), #Signalling
    Symbol("153Eu_beads_4_+p-stat1"), #Signalling
    Symbol("152Sm_P-AKT"), #Signalling
    Symbol("171Yb_pERK1-2"), #Signalling
     Symbol("161Dy_JAK2"),  #Signalling
     Symbol("175Lu_beads_6_pEiF2"), #ER Stress
      Symbol("142Nd_CalR"), #Cell prolif 
     Symbol("172Yb_KI67")]

col_suppl = [Symbol("191Ir_DNA1"), "Event_length", "Center", "Offset", "Width", "Residual"]
cols = vcat(col_surface_markers,col_intracellular_markers);

list_mice = [
            "WT 206", "T1 199", "T2 71", #J2
            "WT 202", "T1 201", "T2 69", #J1
            "WT 239", "T1 238", "T2 132", #J4   
	    "WT 70", "T1 198", "T2 76"  #J3
        ];

list_batch = [2,2,2,
		1,1,1,
		4,4,4,
		3,3,3]

list_WT = ["WT 206", "WT 202", "WT 239", "WT 70"]
list_T1 = ["T1 199", "T1 201", "T1 238", "T1 198"]
list_T2 = ["T2 71", "T2 69", "T2 132", "T2 76"];

list_days = [2,1,4, 3]

pops = ["SLAM", "MPP", "LSK", "LK", "MKP", "MK", "none"];

lim = Dict()
lim["WT 202"] = Dict(
  Symbol("154Sm_CD48")             => 30,
  Symbol("173Yb_c-Kit")            => 141.79,
  Symbol("151Eu_beads_3_CD42d")    => 22.13,
  Symbol("143Nd_CD41")             => 54.31,
  Symbol("164Dy_Sca-1")            => 20,
  Symbol("167Er_CD150")            => 10,
  Symbol("150Nd_CD135")            => 7.71,
);

lim["T1 201"] = Dict(
  Symbol("154Sm_CD48")             => 30,
  Symbol("173Yb_c-Kit")            => 141.64,
  Symbol("151Eu_beads_3_CD42d")    => 22.12,
  Symbol("143Nd_CD41")             => 54.14,
  Symbol("164Dy_Sca-1")            => 20,
  Symbol("167Er_CD150")            => 10,
  Symbol("150Nd_CD135")            => 5.99,
);

lim["T2 69"] = Dict(
  Symbol("154Sm_CD48")             => 30,
  Symbol("173Yb_c-Kit")            => 141.2,
  Symbol("151Eu_beads_3_CD42d")    => 22.36,
  Symbol("143Nd_CD41")             => 54.18,
  Symbol("164Dy_Sca-1")            => 20,
  Symbol("167Er_CD150")            => 10,
  Symbol("150Nd_CD135")            => 6.19,
);

lim["WT 206"] = Dict(
  Symbol("154Sm_CD48")             => 48.49,
  Symbol("173Yb_c-Kit")            => 134.48,
  Symbol("151Eu_beads_3_CD42d")    => 22.06,
  Symbol("143Nd_CD41")             => 54.15,
  Symbol("164Dy_Sca-1")            => 30,
  Symbol("167Er_CD150")            => 10,
  Symbol("150Nd_CD135")            => 3.97,

);

lim["T1 199"] = Dict(
  Symbol("154Sm_CD48")             => 67.72,
  Symbol("173Yb_c-Kit")            => 141.79,
  Symbol("151Eu_beads_3_CD42d")    => 22.08,
  Symbol("143Nd_CD41")             => 54.16,
  Symbol("164Dy_Sca-1")            => 20,
  Symbol("167Er_CD150")            => 10,
  Symbol("150Nd_CD135")            => 2.47,

);

lim["T2 71"] = Dict(
  Symbol("154Sm_CD48")             => 68.05,
  Symbol("173Yb_c-Kit")            => 141.82,
  Symbol("151Eu_beads_3_CD42d")    => 22.06,
  Symbol("143Nd_CD41")             => 54.14,
  Symbol("164Dy_Sca-1")            => 20,
  Symbol("167Er_CD150")            => 10,
  Symbol("150Nd_CD135")            => 2.6,

);

lim["WT 239"] = Dict(
  Symbol("154Sm_CD48")             => 64.12,
  Symbol("173Yb_c-Kit")            => 82.68,
  Symbol("151Eu_beads_3_CD42d")    => 58.45,
  Symbol("143Nd_CD41")             => 51.57,
  Symbol("164Dy_Sca-1")            => 51.63,
  Symbol("167Er_CD150")            => 23.13,
  Symbol("150Nd_CD135")            => 9.89,

);

lim["T1 238"] = Dict(
  Symbol("154Sm_CD48")             => 49.96,
  Symbol("173Yb_c-Kit")            => 68.5,
  Symbol("151Eu_beads_3_CD42d")    => 64.95,
  Symbol("143Nd_CD41")             => 50.12,
  Symbol("164Dy_Sca-1")            => 24.59,
  Symbol("167Er_CD150")            => 15.82,
  Symbol("150Nd_CD135")            => 5.86,

);

lim["T2 132"] = Dict(
  Symbol("154Sm_CD48")             => 49.97,
  Symbol("173Yb_c-Kit")            => 68.46,
  Symbol("151Eu_beads_3_CD42d")    => 64.84,
  Symbol("143Nd_CD41")             => 50.12,
  Symbol("164Dy_Sca-1")            => 24.57,
  Symbol("167Er_CD150")            => 15.82,
  Symbol("150Nd_CD135")            => 4.51,

);

lim["WT 70"] = Dict(
  Symbol("154Sm_CD48")             => 30.0,
  Symbol("173Yb_c-Kit")            => 12.0,
  Symbol("151Eu_beads_3_CD42d")    => 20,
  Symbol("143Nd_CD41")             => 55,
  Symbol("164Dy_Sca-1")            => 30,
  Symbol("167Er_CD150")            => 22,
  Symbol("150Nd_CD135")            => 5.62,

);

lim["T1 198"] = Dict(
  Symbol("154Sm_CD48")             => 30.0,
  Symbol("173Yb_c-Kit")            => 12.0,
  Symbol("151Eu_beads_3_CD42d")    => 20,
  Symbol("143Nd_CD41")             => 55,
  Symbol("164Dy_Sca-1")            => 45,
  Symbol("167Er_CD150")            => 35,
  Symbol("150Nd_CD135")            => 5.62,

);

lim["T2 76"] = Dict(
  Symbol("154Sm_CD48")             => 30.0,
  Symbol("173Yb_c-Kit")            => 12.0,
  Symbol("151Eu_beads_3_CD42d")    => 20,
  Symbol("143Nd_CD41")             => 55,
  Symbol("164Dy_Sca-1")            => 55,
  Symbol("167Er_CD150")            => 35,
  Symbol("150Nd_CD135")            => 10,

);


