######################################
#### Cell selection ##################
######################################
import Pkg
Pkg.activate("/home/gurvan/julia/project_SingleCells")
using CSV, DataFrames
using Statistics
using Distributions
using Distances
using NearestNeighborDescent
using Dates, JLD2, HDF5, FileIO

include("config_mice.jl")
nb_traj = 1


df = Dict()
include("load_mice.jl")


mouse = list_T1[3]
df[mouse] = df[mouse][df[mouse][:,:pop] .!= "none",:];



A_raw = Matrix(df[mouse][:,cols])
A = asinh.(A_raw);
mean_A = mean(A, dims=1)
A ./= mean_A

N_cells = size(A,1)

n_neighbors_rw = 20
metric = Euclidean()
graph_rw = NearestNeighborDescent.DescentGraph(A', n_neighbors_rw, metric) 
indices_rw = graph_rw.indices
distances_rw = graph_rw.distances
#Or
#graph_rw = nndescent(A', n_neighbors_rw, metric)
#indices_rw, distances_rw = knn_matrices(graph_rw)

prob_voisin = [Categorical(distances_rw[:,i]./sum(distances_rw[:,i])) for i in 1:N_cells];


cat_cells = zeros(Int, N_cells)
for i in 1:N_cells
    c = df[mouse][i,:pop]
    if c == "SLAM"
        cat_cells[i] = 0
    elseif c == "MPP"
        cat_cells[i] = 1
    elseif c == "LSK"
        cat_cells[i] = 2
    elseif c == "LK"
        cat_cells[i] = 3
    elseif c =="MKP"
        cat_cells[i] = 4
    elseif c == "MK"
        cat_cells[i] = 5
    else
        println("error")
    end
end


Trajectoires = Array{Any}(undef, nb_traj)



println("begin loop")
for k in 1:nb_traj
   
    ind_begin = rand(collect(1:N_cells)[df[mouse][:,:pop] .== "SLAM"])
    ind_end = rand(collect(1:N_cells)[df[mouse][:,:pop] .== "MK"])
    
    cycle = true
    
    nb_in_cycle = 0
    while cycle
        nb_in_cycle += 1

        traj = zeros(Int, N_cells);
        traj[1] = ind_begin;

        expl = zeros(Int,  N_cells);

        for i in 1:N_cells-1
            actuel = traj[i]
            expl[actuel] += 1

            not_explored = [expl[indices_rw[j,actuel]] for j in 1:n_neighbors_rw]
            if !(0 in not_explored)
                #Tous les voisins ont déjà été explorés ou retour vers une pop antérieure
                #On recommence depuis le début
                break
            end
            num = rand(prob_voisin[actuel])
            traj[i+1] = indices_rw[num, actuel]

            #On ne garde la nouvelle cellule que si elle n'a pas été explorée
            ind_security = 0
            lim_security = 1_000
            while not_explored[num] == 1 && ind_security < lim_security
                #Si la nouvelle cellule a déjà été visitée, on en retire une
                num = rand(prob_voisin[actuel])
                traj[i+1] = indices_rw[num, actuel]
                ind_security += 1
            end
            if ind_security == lim_security
                #Si au bout de X essais, on n'a pas tiré le plus proche voisin, on les explore dans l'ordre
                for v in 1:n_neighbors_rw
                    if not_explored[v] == 0
                        traj[i+1] = indices_rw[v, actuel]
                        break
                    end
                end
            end
            
           #Est-ce qu'on a atteint la cible ? 
           if traj[i+1] == ind_end
                cycle = false 
                #On enregistre la trajectoire
                Trajectoires[k] = traj[1:i+1]
                break
            end
            
           #Si on passe dans une nouvelle catégorie, on marque comme explorée toutes les cellules antérieures
            if cat_cells[traj[i+1]] > cat_cells[traj[i]]
               for c in cat_cells[traj[i]]:pop_cells[traj[i+1]]-1
                    expl[collect(1:N_cells)[cat_cells.==c]] .= 1
                end
            end
            
        end
       
        #Si on a été bloqué, cycle = true et on recommence

        if nb_in_cycle > 10_000
            cycle = false
            k = k-1
        end
    end
            
         
    
end


save(string("traj/T_",mouse,"_",nb_traj,"_",now(),".jld2"),"T",Trajectoires )


