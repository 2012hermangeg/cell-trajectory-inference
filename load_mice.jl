for mouse in list_mice
    df[mouse] = CSV.read(string("data/",mouse,".csv"), DataFrame,  delim=',', decimal='.')
    df[mouse]  = df[mouse][df[mouse][!,:Center].>0.01,:]

    df[mouse][!,:pop] =  Array{String}(undef, size(df[mouse],1))
    
   for i in 1:size(df[mouse],1)
    line = df[mouse][i,:]
        if line[Symbol("173Yb_c-Kit") ] < lim[mouse][Symbol("173Yb_c-Kit") ] #c-kit neg
            if line[Symbol("143Nd_CD41")] >= lim[mouse][Symbol("143Nd_CD41")] &&
                line[Symbol("151Eu_beads_3_CD42d")] >= lim[mouse][Symbol("151Eu_beads_3_CD42d")]
                
                df[mouse][i,:pop] = "MK"
                
            else
                 df[mouse][i,:pop] = "none"
            end
        else #c-kit +
            if line[Symbol("164Dy_Sca-1") ] < lim[mouse][Symbol("164Dy_Sca-1")  ] #Sca neg
                
                if line[Symbol("143Nd_CD41")] >= lim[mouse][Symbol("143Nd_CD41")] &&
                    line[Symbol("151Eu_beads_3_CD42d")] >= lim[mouse][Symbol("151Eu_beads_3_CD42d")]
                
                    df[mouse][i,:pop] = "MK"
                    
                elseif line[Symbol("143Nd_CD41")] >= lim[mouse][Symbol("143Nd_CD41")] &&
                    line[Symbol("167Er_CD150") ] >= lim[mouse][Symbol("167Er_CD150") ] #CD150+
                    
                    df[mouse][i,:pop] = "MKP"
                    
                else
                    df[mouse][i,:pop] = "LK"
                end
            else
                if line[Symbol("167Er_CD150") ] >= lim[mouse][Symbol("167Er_CD150") ] && #CD150+
                    line[Symbol("154Sm_CD48") ] < lim[mouse][Symbol("154Sm_CD48") ] #CD48-
                    
                    df[mouse][i,:pop] = "SLAM"
                    
                elseif line[Symbol("150Nd_CD135") ] < lim[mouse][Symbol("150Nd_CD135") ] && #CD135-
                    line[Symbol("154Sm_CD48") ] >= lim[mouse][Symbol("154Sm_CD48") ] #CD48+
                    
                    df[mouse][i,:pop] = "MPP"
                    
                else
                    df[mouse][i,:pop] = "LSK"
                end
            end
            
        end
    end
    
    
    
end
