######################################
#### Cell selection ##################
######################################
import Pkg
Pkg.activate("/home/gurvan/julia/project_SingleCells")
using CSV, DataFrames
using Plots
using Statistics
using Distributions, Random
using Distances
using UMAP
using Dates, JLD2, HDF5, FileIO
using Interpolations


include("config_mice.jl")
choice_plot = ""

#Load dataframe with data into df
df = Dict()
include("load_mice.jl")


list_genotypes = ["WT", "T1", "T2"]
list_list_genotype = [list_WT, list_T1, list_T2]


#########################
## Load infered trails ##
#########################

nb_trails_per_mouse = 100_000;

N_cells = Dict()
pop_cells = Dict()
dict_trails_raw = Dict()

println()
println("### Load trails ###")
for mouse in list_mice
    df[mouse] = df[mouse][df[mouse][:,:pop] .!= "none",:];
    N_cells[mouse] = size(df[mouse],1)
    pop_cells[mouse] = zeros(Int, N_cells[mouse])
    for i in 1:N_cells[mouse]
        c = df[mouse][i,:pop]
        if c == "SLAM"
            pop_cells[mouse][i] = 0
        elseif c == "MPP"
            pop_cells[mouse][i] = 1
        elseif c == "LSK"
            pop_cells[mouse][i] = 2
        elseif c == "LK"
            pop_cells[mouse][i] = 3
        elseif c =="MKP"
            pop_cells[mouse][i] = 4
        elseif c == "MK"
            pop_cells[mouse][i] = 5
        else
            println("error")
        end
    end
    cd(string("traj/",replace(mouse, " " => "_"),"/"))
    trail_names = readdir()
    
    nb_trails = 0
    for n in trail_names
        nb_trails += parse(Int64,string(split(n, "_")[3]))
    end

    dict_trails_raw[mouse] = Array{Any}(undef, nb_trails)

    tt = 1
    Random.seed!(1)
    for n in shuffle!(trail_names)
        T = load(n)["T"]
        for t in 1:length(T)
            try 
                dict_trails_raw[mouse][tt] = T[t]
                tt += 1
            catch e
            end
        end
    end
    println(string(mouse," - ", tt))
    dict_trails_raw[mouse] = dict_trails_raw[mouse][1:nb_trails_per_mouse]; #Select a limited nb of trails
    
    
    cd("../../")
end
println()
println("#### Trail normalisation ####")

# Taking into account distances between cells
dict_trails_D = Dict() #Absolute distance D (euclidean)
for m in list_mice
    
    A_raw = Matrix(df[m][:,cols])
    A = asinh.(A_raw);
    mean_A = mean(A, dims=1)
    A ./= mean_A
    
    dict_trails_D[m] = Array{Any}(undef, nb_trails_per_mouse)
    for i in 1:nb_trails_per_mouse
        t = dict_trails_raw[m][i]
        Nt = length(t)
        dict_trails_D[m][i] = zeros(Nt)
        for j in 2:Nt
            dict_trails_D[m][i][j] = dict_trails_D[m][i][j-1] + 
                                Distances.evaluate(Euclidean(), A[t[j-1],:], A[t[j],:])
        end
    end

end

#Normalisation of the trails
dict_trails_normalize_d = Dict() #Relative distance d in [0,1]
for m in list_mice
    
    dict_trails_normalize_d[m] = deepcopy(dict_trails_D[m])
    
    for i in 1:nb_trails_per_mouse
        dict_trails_normalize_d[m][i] ./= dict_trails_normalize_d[m][i][end]

    end
    
end

#Caracterising trails with 5 values, transitions between populations
dict_trails_expansion_d = Dict()
for m in list_mice
    dict_trails_expansion_d[m] = zeros(nb_trails_per_mouse, 5)
    for i in 1:nb_trails_per_mouse
        for c in 0:4
            ind = pop_cells[m][dict_trails_raw[m][i]] .== c
            if sum(ind) == 0.0
                dict_trails_expansion_d[m][i,c+1] = dict_trails_expansion_d[m][i,c]
            else
                dict_trails_expansion_d[m][i,c+1] = dict_trails_normalize_d[m][i][findlast(ind)+1]
            end
        end
    end
end

choice_plot = "corr_occup_LK_over_length"
include("path_inference_plots.jl")

################################
### Filter - too long trails ###
################################

println()
println("#### Trail filtering ####")

# Define max trail length

length_trail_lim = Dict( #Initialize
    "T1 199" => 500,
    "T1 201" => 500,
    "T1 238" => 1000,
    
    "T2 69" => 300,
    "T2 71" => 480,
    "T2 132" => 2600,
    
    "WT 202" => 300,
    "WT 206" => 700,
    "WT 239" => 300,

    "WT 70" => 100,
    "T1 198" => 100,
    "T2 76" => 100
)


threshold_filter_max = 0.4
for mouse in list_mice
    mean_occup_LK = 100 .*[dict_trails_expansion_d[mouse][i,4].-dict_trails_expansion_d[mouse][i,3] for i in 1:nb_trails_per_mouse]
    length_trail = [dict_trails_D[mouse][i][end] for i in 1:nb_trails_per_mouse]
    
    A = hcat(length_trail, mean_occup_LK)
    x = collect(1:maximum(length_trail))
    

    for t in length_trail_lim[mouse]:maximum(length_trail)
        if cor(A[A[:,1] .<t,:])[1,2] > threshold_filter_max
            length_trail_lim[mouse] = t-1
            break
        end
    end
end

println()
println("Show length trail lim")
@show length_trail_lim

choice_plot = "scatter_length_occupLK_filterMax"
include("path_inference_plots.jl")
choice_plot = "length_trail_max"
include("path_inference_plots.jl")
choice_plot = "corr_occup_LK_over_length_tresh"
include("path_inference_plots.jl")

ind_filtre_max = Dict()
for m in list_mice
    s_traj = map(x -> x[end], dict_trails_D[m])
    ind_filtre_max[m] = s_traj.<= length_trail_lim[m]
    @show (m, sum(ind_filtre_max[m]))
end

choice_plot = "occup_LK_filter_max"
include("path_inference_plots.jl")

#################################
### Filter - too short trails ###
#################################


# Sélection final trails

ind_final_trails = Dict()
ind_filtre_min = Dict()

threshold_filter_min = 0.01
println()
println("Show nb of finally selected trails:")
for mouse in list_mice

    occup_LK =[dict_trails_D[mouse][i][end]*(
                    dict_trails_expansion_d[mouse][i,4].-dict_trails_expansion_d[mouse][i,3]
                    ) for i in 1:nb_trails_per_mouse]
       
    ind_filtre_min[mouse] = occup_LK .> threshold_filter_min* length_trail_lim[mouse]    
    ind_final_trails[mouse] = ind_filtre_min[mouse] .* ind_filtre_max[mouse]
    
    @show (mouse, sum(ind_final_trails[mouse]))

    
end

choice_plot = "occup_LK_final_trails"
include("path_inference_plots.jl")

choice_plot = "scatter_length_occupLK_filterAll"
include("path_inference_plots.jl")

################################
### Summary - Filtering      ###
################################


for m in list_mice
    nb_times_in_traj = zeros(Int, N_cells[m])

    for t in dict_trails_raw[m][ind_final_trails[m]]
        z = zeros(Int, N_cells[m])
        z[t] .= 1
        nb_times_in_traj .+= z
    end
    df[m][!,:in_selected_trails] = nb_times_in_traj
end


choice_plot = "hist_cells_filtering"
include("path_inference_plots.jl")



################################
### Exploratory path         ###
################################

println()
println("#### Infer exploratory paths ####")

arr_r = collect(0.0:0.001:1.0);
arr_r_bis = collect(0.0:0.01:1.0);

expansion = Dict()
for m in list_mice
     pdf_SLAM = [ mean(sqrt.(dict_trails_expansion_d[m][ind_final_trails[m],1]) .> r) for r in arr_r]

    pdf_LSK = [ mean((sqrt.(dict_trails_expansion_d[m][ind_final_trails[m],3]) .> r).*(
                sqrt.(dict_trails_expansion_d[m][ind_final_trails[m],1]) .<= r)) for r in arr_r]

    pdf_LK = [ mean((sqrt.(dict_trails_expansion_d[m][ind_final_trails[m],5]) .> r).*(
                sqrt.(dict_trails_expansion_d[m][ind_final_trails[m],3]) .<= r)) for r in arr_r]

    pdf_MK = [ mean(sqrt.(dict_trails_expansion_d[m][ind_final_trails[m],5]) .<= r) for r in arr_r]
    
    expansion[m] = 0 .* pdf_SLAM + 1/3 .*pdf_LSK + 2/3 .*pdf_LK +1 .*pdf_MK
end



expansion["pool_WT"] = zeros(length(expansion[list_mice[1]]))

for m in list_WT
  expansion["pool_WT"] .+= expansion[m]
end
expansion["pool_WT"] ./= length(list_WT)


map_r = Dict()

for i in 1:length(list_days)
    m = list_WT[i]
    map_r[list_days[i]] = map(y-> arr_r[findmax(expansion["pool_WT"].>=y)[2]],expansion[m])
    ind_max = findmax(map_r[list_days[i]] .> 0)[2]
    val_first = map_r[list_days[i]][ind_max]
    for ii in 2:ind_max-1
        map_r[list_days[i]][ii] = ii/ind_max*val_first
    end
end



choice_plot = "exploratory_path_ind" 
include("path_inference_plots.jl")

choice_plot = "exploratory_path_all" 
include("path_inference_plots.jl")

choice_plot = "exploratory_path_WT_pool" 
include("path_inference_plots.jl")

#Save map_r_batch
println()
println("Save map r")
for i in 1:length(list_days)
  CSV.write(string("sav/map_r_batch_J",list_days[i],".csv"), DataFrame(v=map_r[list_days[i]]))
end








################################
### Differentiation path     ###
################################

println()
println("#### Infer differentiation paths ####")
println()


for j in 1:length(cols)

  println(string(cols[j]))

  traj_diff_j = Dict()
  for m in list_mice
    
    traj_diff_j[m] = zeros(sum(ind_final_trails[m]), length(arr_r_bis))
    i = 0
    for k in 1:length(ind_final_trails[m])
        if ind_final_trails[m][k] == 1
            i += 1

            int_marker = map(x-> asinh(df[m][x,cols[j]]), dict_trails_raw[m][k])

            for i_r in 1:length(arr_r_bis)
                r = arr_r_bis[i_r]
                ind_mean = (dict_trails_normalize_d[m][k] .>= r -0.03).*(
                                        dict_trails_normalize_d[m][k] .<= r +0.03)
                if sum(ind_mean) == 0
                    traj_diff_j[m][i,i_r] = traj_diff_j[m][i,i_r]
                else
                    traj_diff_j[m][i,i_r] = mean(int_marker[ind_mean])
                end
            end
        end
    end
  end

  mean_traj = Dict()
  inf_traj = Dict()
  sup_traj = Dict()

  for m in list_mice
    day = list_batch[findmax(list_mice.==m)[2]]
    interp = LinearInterpolation(map_r[day][1:10:end], 
    mean(traj_diff_j[m], dims=1)'[1:end])
    
    mean_traj[m] = interp(arr_r)
  end

  for m in list_mice
    day = list_batch[findmax(list_mice.==m)[2]]
    interp = LinearInterpolation(map_r[day][1:10:end], 
    [quantile(traj_diff_j[m][:,i], [0.10])[1] for i in 1:size(traj_diff_j[m],2)])
    
    inf_traj[m] = interp(arr_r)
  end

  for m in list_mice
    day = list_batch[findmax(list_mice.==m)[2]]
    interp = LinearInterpolation(map_r[day][1:10:end], 
     [quantile(traj_diff_j[m][:,i], [0.9])[1] for i in 1:size(traj_diff_j[m],2)])
    
    sup_traj[m] = interp(arr_r)
  end

  mean_traj["pool_WT"] = zeros(length(mean_traj[list_mice[1]]))

  for m in list_WT
    mean_traj["pool_WT"] .+= mean_traj[m]
  end
  mean_traj["pool_WT"] ./= length(list_WT)

  lambda_j = Dict()
  for i in 1:length(list_days)
    m = list_WT[i]
    lambda_j[list_days[i]] = mean_traj["pool_WT"]./ mean_traj[m] 
  end

  #Save
  for m in list_mice
    CSV.write(string("sav/traj_",cols[j],"_",m,".csv"), 
               DataFrame(mean=mean_traj[m],inf=inf_traj[m], sup=sup_traj[m]) )
  end
  for i in 1:length(list_days)
    CSV.write(string("sav/lambda_batch_J",list_days[i],"_",cols[j],".csv"), DataFrame(v=lambda_j[list_days[i]]))
  end

  ###########
  ## Plots ##
  plot(title=string(cols[j]))

  #before y-correction
  for i in 1:length(list_WT)
    m = list_WT[i]

    plot!(arr_r, mean_traj[m], 
        linewidth=2, color=:black, linestyle=list_style[1+(i-1)*3], label="")

  end

  plot!(arr_r,mean_traj["pool_WT"] , linewidth=4, color=:purple, label="pool", legend=:bottomright)

  # After y-correction
  for i in 1:length(list_WT)
    m = list_WT[i]
    
    plot!(arr_r,mean_traj[m].*lambda_j[list_days[i]], label="", linestyle=:dash, color=:darkblue)

  end

  plot!(xlabel="r", ylabel="Intensity", )

  savefig(string("fig/differentiation/correction/correction_",cols[j]))
  

  ### Without realigning y-axis
  plot()
  for i in 1:length(list_mice)
    m = list_mice[i]

    plot!( arr_r,mean_traj[m], 
        linewidth=3, color=list_col[i], linestyle=list_style[i], label="")

  end

  plot!(xlabel="r", ylabel="Intensity", title=string(cols[j], " - Original"))
  savefig(string("fig/differentiation/before_corr_y/traj_",cols[j]))

  ### With corrected y-axis
  plot()
  for i in 1:length(list_mice)
    m = list_mice[i]

    plot!(arr_r,mean_traj[m].*lambda_j[list_batch[i]], 
        linewidth=3, color=list_col[i], linestyle=list_style[i], label="")

  end

  plot!(xlabel="r", ylabel="Intensity", title=string(cols[j], " - correction batch"))
  savefig(string("fig/differentiation/after_corr_y/traj_",cols[j]))




  plot()

  plot!(arr_r, mean_traj["pool_WT"], linewidth=3, color=col_genotypes[1], label="")


  pool_T1 = zeros(length(mean_traj["pool_WT"]))
  for i in 1:length(list_T1)
    m = list_T1[i]
    pool_T1 .+= mean_traj[m].*lambda_j[list_days[i]]
  end
  pool_T1 ./= length(list_T1)

  plot!(arr_r, pool_T1, linewidth=3, color=col_genotypes[2], label="")

  pool_T2 = zeros(length(mean_traj["pool_WT"]))
  for i in 1:length(list_T2)
    m = list_T2[i]
    pool_T2 .+= mean_traj[m].*lambda_j[list_days[i]]
  end
  pool_T2 ./= length(list_T2)

  plot!(arr_r, pool_T2, linewidth=3, color=col_genotypes[3], label="")

  plot!(xlabel="r", ylabel="Intensity", title=string(cols[j]))

  savefig(string("fig/differentiation/pool/pool_",cols[j]))
  #col_genotypes = [:black,:red,:green] #WT, T1, T2
end







